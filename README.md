# Parse Nginx config file

```
import nginx_config_parser

with open("example.conf") as conf:
    context = nginx_config_parser.parse(conf)
    
print(context.directives)
```

## Testing
```
python -m unittest tests.unit
```