from distutils.core import setup

setup(name='nginx-config-parser',
      version='0.1.0',
      description='Parse Nginx config files',
      author='Raphaël JOIE',
      author_email='raphael.joie@ext.ec.europa.eu',
      url='https://www.python.org/sigs/distutils-sig/',
      packages=['nginx_config_parser'],
      package_dir={'nginx_config_parser': 'src/nginx_config_parser'},
      )
