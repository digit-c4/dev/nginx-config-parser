from .context import Context


def parse(file):
    context = Context()

    for line in file:
        for ch in line:
            if ch == "#":
                break
            elif ch == ";":
                if context.last is not None:
                    context.last.close()
            elif ch == "{":
                if context.last is None or context.last.was_closed:
                    raise Exception("Context can only be opened after a directive")

                context.last.context = Context(parent=context)
                context = context.last.context
            elif ch == "}":
                if context.parent is None:
                    raise Exception("Unexpected context closing")
                context = context.parent
                context.last.close()

            elif ch == "\n":
                pass
            elif ch in [" ", "\t"] and (context.last is None or context.last.was_closed):
                pass
            else:
                context.append(ch)

    return context
