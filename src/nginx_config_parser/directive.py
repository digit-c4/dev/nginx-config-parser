from typing import Union


class Directive:
    def __init__(self, raw: str = None):
        self.raw: Union[str, None] = raw
        self.context = None
        self.complete: bool = False if raw is None else True
        self._directive: Union[str, None] = None
        self._parameters: Union[list[str], None] = None

    @property
    def directive(self) -> str:
        if not self.complete:
            raise Exception("Directive was not closed")

        if self._directive is None:
            s = self.raw.split(" ")
            self._directive = s[0]

        return self._directive

    @property
    def parameters(self) -> list[str]:
        if not self.complete:
            raise Exception("Directive was not closed")

        if self._parameters is None:
            s = self.raw.split(" ")
            self._parameters = s[1:]

        return self._parameters

    def close(self):
        self.complete = True

    @property
    def was_closed(self) -> bool:
        return self.complete

    def __str__(self) -> str:
        return f"{self.raw}"

    def append(self, ch):
        if self.was_closed:
            raise Exception("Cannot append char to closed directive")
        if self.raw is None: self.raw = ""
        self.raw += ch
