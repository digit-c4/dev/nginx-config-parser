from .directive import Directive


class Context:
    def __init__(self, parent=None):
        self.parent: Context = parent
        self.directives: list[Directive] = []

    def append(self, ch):
        if self.last is None or self.last.was_closed:
            self.directives += [Directive()]

        self.last.append(ch)

    @property
    def last(self):
        if len(self.directives) == 0:
            return None
        return self.directives[len(self.directives) - 1]

    def get(self, directive: str) -> list[Directive]:
        return list(filter(lambda d: d.directive == directive, self.directives))
