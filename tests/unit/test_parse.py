import json
import unittest

from nginx_config_parser.parse import parse


class TestParse(unittest.TestCase):

    def test_parse(self):
        with open("tests/unit/example.conf") as conf:
            context = parse(conf)
