import unittest

from nginx_config_parser.context import Context
from nginx_config_parser.directive import Directive


class TestContext(unittest.TestCase):

    def test_get(self):
        c = Context()
        d1 = Directive(raw="location /test")
        d2 = Directive(raw="blabetiblou unknown")
        d3 = Directive(raw="location unknown")
        c.directives = [d1, d2, d3]
        self.assertEqual(c.get("location"), [d1, d3])
