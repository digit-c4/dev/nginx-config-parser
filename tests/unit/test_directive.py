import unittest

from nginx_config_parser.directive import Directive


class TestDirective(unittest.TestCase):

    def test_init(self):
        d = Directive()
        self.assertIsNotNone(d)
        self.assertFalse(d.was_closed)
        self.assertIsNone(d.raw)
        self.assertIsNone(d.context)

    def test_init_raw(self):
        d = Directive("blabetiblou lol")
        self.assertIsNotNone(d)
        self.assertTrue(d.was_closed)
        self.assertEqual(d.raw, "blabetiblou lol")
        self.assertEqual(d.directive, "blabetiblou")
        self.assertEqual(d.parameters, ["lol"])
        self.assertIsNone(d.context)

    def test_close(self):
        d = Directive()
        d.close()
        self.assertTrue(d.was_closed)

    def test_append(self):
        d = Directive()
        d.append("a")
        d.append("b")
        self.assertEqual(d.raw, "ab")

    def test_append_closed(self):
        d = Directive()
        d.close()
        with self.assertRaises(Exception):
            d.append("2")

    def test_directive(self):
        d = Directive()
        d.append("blabetiblou")
        d.close()
        self.assertEqual(d.directive, "blabetiblou")

    def test_parameters(self):
        d = Directive()
        d.append("blabetiblou azerty qwerty")
        d.close()
        self.assertEqual(d.parameters, ["azerty", "qwerty"])
